原作者模型：
链接：https://pan.baidu.com/s/1LDidGwGK33JTJkLuH5lmnQ 
提取码：fq0m 


20190619 模型
链接：https://pan.baidu.com/s/1Iy67qcXvkNUfjdUktl7Wtw
提取码：285t
50 epoch： Accuracy: 0.98183+-0.00486   Validation rate: 0.83133+-0.03152 @ FAR=0.00133


安装依赖，pip install -r requirements.txt
注，如果没有英伟达显卡，请修改requirements.txt中tensorflow-gpu为tensorflow


1.运行embedding_faces_data.py 做人脸对齐，以及生成人脸编码数据，用于人脸对比。
   修改文件内最下方文件路径。其中原始照片需要按照姓名份文件夹存放，程序会遍历每个文件夹做人脸裁剪。
2.使用capture_face.py做视频人脸识别。
   修改文件内第15行embedding.pk文件路径为步骤1生成的pk 文件路径。


原始照片目录结构参考：
photos
    |__name1
    |     |1.jpg
    |     |2.jpg
    |__name2
          |1.jpg
          |2.jpg


备注：
本代码基于https://github.com/davidsandberg/facenet 精简而来，将对齐和识别放到了一起，模型使用lfw数据集在colab上训练了20个epoch。
目前模型的验证结果如下：
Accuracy: 0.96067+-0.00667
Validation rate: 0.64800+-0.03947 @ FAR=0.00100
Area Under Curve (AUC): 0.994
Equal Error Rate (EER): 0.039

后续继续训练更好的模型后再更新。